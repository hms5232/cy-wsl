sudo sysctl -w net.ipv4.conf.all.route_localnet=1
sudo iptables -t nat -I PREROUTING -p tcp -j DNAT --to-destination 127.0.0.1
echo
sudo /etc/init.d/mysql start
sudo service php7.4-fpm start
sudo service php8.1-fpm start
sudo service nginx start
sudo service docker start
sudo service supervisor start
sudo service redis-server start
echo
sudo service cron start
sudo service atd start
echo
sudo netstat -ntupl
echo
#docker run --rm -d --name hoppscotch -p 3000:3000 hoppscotch/hoppscotch:latest
echo

