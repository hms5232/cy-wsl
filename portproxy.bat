@echo off
set /p port="portproxy: "
@echo on
netsh interface portproxy set v4tov4 listenport=%port% connectaddress=localhost connectport=%port% listenaddress=0.0.0.0 protocol=tcp
@echo off
