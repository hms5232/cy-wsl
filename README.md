# CY-wsl

## 參考資料
* [Windows 10 上適用於 Linux 的 Windows 子系統安裝指南](https://docs.microsoft.com/zh-tw/windows/wsl/install-win10)
* [使用 WSL 2 打造優質的多重 Linux 開發環境](https://blog.miniasp.com/post/2020/07/26/Multiple-Linux-Dev-Environment-build-on-WSL-2)
* [開始使用 Windows 子系統 Linux 版上的資料庫](https://docs.microsoft.com/zh-tw/windows/wsl/tutorials/wsl-database)
* https://github.com/shayne/wsl2-hacks

## submodule
### clone
```
git clone <repo url> --recurse-submodules
```
有做這一步就不需要再初始化

### 初始化
```
git submodule init
git submodule update
```

### 更新
```
git submodule update --recursive --remote
```
或
```
git submodule foreach git pull
```
或
```
git pull --recurse-submodules
```

### 移除
> 參考資料：https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule

```
git submodule deinit <your-submodule-path | --all>  # 清除 .git/config 相關設定
rm -r .git/modules/<your-submodule-path>
git rm <your-submodule-path>  # 幫你清專案裡的 submodule 資料夾及 .gitsubmodule 中相關設定
```
